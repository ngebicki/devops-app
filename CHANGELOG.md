# [1.5.0](https://gitlab.com/ngebicki/devops-app/compare/v1.4.1...v1.5.0) (2023-09-16)


### Bug Fixes

* bumped node image for semantic-release ([2326dd7](https://gitlab.com/ngebicki/devops-app/commit/2326dd75223f69826e886c830f67e43b9a21c9cb))


### Features

* added deployment step ([adcfb1f](https://gitlab.com/ngebicki/devops-app/commit/adcfb1fb1fbca5ba9a6b66503ecefde35d3ff2f0))

## [1.4.1](https://gitlab.com/ngebicki/devops-app/compare/v1.4.0...v1.4.1) (2023-09-14)


### Bug Fixes

* cleanup code, added tag to dockerfile base image ([387236c](https://gitlab.com/ngebicki/devops-app/commit/387236c297b4ac859d1720085ac7f416945f23b2))

# [1.4.0](https://gitlab.com/ngebicki/devops-app/compare/v1.3.1...v1.4.0) (2023-09-14)


### Bug Fixes

* added modules ([18f0514](https://gitlab.com/ngebicki/devops-app/commit/18f0514c6f5f1ca2b0a308230d40ef57283a434a))


### Features

* added prometheus metrics ([bbabfc4](https://gitlab.com/ngebicki/devops-app/commit/bbabfc4bb0dc5fafeedace19d2feb26c4e7411d3))
