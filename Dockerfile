FROM alpine:3.18.3

ARG USERNAME=devops-user
ARG USER_UID=1000

# Create the user
RUN adduser -D -u $USER_UID $USERNAME \
    && apk add libc6-compat

EXPOSE 8080/tcp

COPY devops-release /bin/devops-release

USER $USERNAME
CMD ["/bin/devops-release"]

